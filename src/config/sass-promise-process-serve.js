const sass = require('node-sass-promise');
const fse = require("fs-extra");
const path = require("path");
const chokidar = require("chokidar"); 

module.exports =( ( scssPath, cssPath ) => {
  
  if( !fse.existsSync( path.dirname( cssPath ) ) ) {
    
    // console.log( "CREATE A NEW CSS FILE..." );
    // Create cssPath directory recursively, then, render css from sass, then, write result css string to cssPath file

    fse.mkdir( path.dirname( cssPath ), { recursive: true } )
      .then( () => {
        let renderobject = { file: scssPath, outputStyle: "compressed" };
        return sass.render( renderobject );
      } )
      .then( ( result ) => {
        
        // return fse.writeFile( cssPath, result.css.toString() );

        let stringOne = `@charset "UTF-8";`; // console.log( "stringOne" , stringOne );
        let stringTwo = result.css.toString(); // console.log( "stringTwo" , stringTwo );
        let wholeString = stringOne.concat(stringTwo);
        return fse.writeFile( cssPath, wholeString );

      } )
      .catch( (err) => { console.error( "err: " , err ) } );

  }

  const watcher = chokidar.watch( path.dirname( scssPath , { persistent: true , recursive: true } ) );

  // Watch for changes to scssPath directory...

  watcher.on( 'change' , () => {
    
    // Render css from sass, then, write result css string to cssPath file
    let renderobject = { file: scssPath, outputStyle: "compressed" };
    
    sass.render( renderobject )
    .then( ( result ) => {
      
      // return fse.writeFile( cssPath, result.css.toString() );

      let stringOne = `@charset "UTF-8";`; // console.log( "stringOne" , stringOne );
      let stringTwo = result.css.toString(); // console.log( "stringTwo" , stringTwo );
      let wholeString = stringOne.concat(stringTwo);
      return fse.writeFile( cssPath, wholeString );

    } )
    .catch( (err) => { console.error( "err: " , err ) } );

  } );

} ); // CREATIVELY MOTIVATED BY https://dev.to/mathieuhuot/processing-sass-with-11ty-5a09 