const sass = require('node-sass-promise');
const fse = require("fs-extra");
const path = require("path");

module.exports =( ( scssPath, cssPath ) => {
  
  fse.pathExists( path.dirname( cssPath ) , (err,exists) => {
    
    // console.log( "err", err ); // null
    // console.log( "exists", exists ); // true or false

    if( exists === false ){
      
      // console.log( "CREATE A NEW CSS FILE..." );

      // Create cssPath directory recursively, then, render css from sass, then, write result css string to cssPath file

      fse.mkdir( path.dirname( cssPath ), { recursive: true } )
      .then( () => {
        let renderobject = { file: scssPath, outputStyle: "compressed" };
        return sass.render( renderobject );
      } )
      .then( ( result ) => {

        // return fse.writeFile( cssPath, result.css.toString() );

        let stringOne = `@charset "UTF-8";`; // console.log( "stringOne" , stringOne );
        let stringTwo = result.css.toString(); // console.log( "stringTwo" , stringTwo );
        let wholeString = stringOne.concat(stringTwo);
        return fse.writeFile( cssPath, wholeString );
        
      } )
      .then( () => {
        // console.log( "path.dirname( cssPath ): ", path.dirname( cssPath ) ); // src/css
        let fromSrc = `${path.dirname( cssPath )}/styles.css`;
        let toDist = `dist/css/styles.css`;
        fse.createReadStream( fromSrc ).pipe( fse.createWriteStream( toDist ) );
        // console.log( "COUNT IT!!!" );
        
      } )
      .catch( (err) => { console.error( "err: " , err ) } );

    }

  } );

} ); // CREATIVELY MOTIVATED BY https://dev.to/mathieuhuot/processing-sass-with-11ty-5a09 