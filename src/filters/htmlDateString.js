/* #####
# https://github.com/11ty/eleventy-base-blog/blob/master/.eleventy.js
# https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
##### */

const { DateTime } = require("luxon");

module.exports = ( ( dateObj ) => {
  return DateTime.fromJSDate( dateObj, { zone: 'utc' } ).toFormat("dd LLL yyyy");
} );