/* #####
# https://github.com/11ty/eleventy-base-blog/blob/master/.eleventy.js
##### */

const { DateTime } = require("luxon");

module.exports = ( (dateObj) => {
  return DateTime.fromJSDate( dateObj, { zone: 'utc' } ).toFormat("yyyy-LL-dd");
} );