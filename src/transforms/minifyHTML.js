/* #####
# minified html output
# https://www.11ty.dev/docs/config/#transforms 
# https://www.npmjs.com/package/html-minifier
# https://david-dm.org/kangax/html-minifier
    # https://www.npmjs.com/package/camel-case
    # https://www.npmjs.com/package/param-case
    # https://www.npmjs.com/package/commander
##### */

const htmlmin = require("html-minifier");

module.exports = ( ( content, outputPath ) => {
  if( outputPath.endsWith(".html")){
    let minified = htmlmin.minify( content , {
      useShortDoctype: true
      , removeComments: true
      , collapseWhitespace: true
    } );
    return minified;
  }
  return content;
} );