module.exports = ( ( collection ) => {
  
  let tagSet = new Set();

  collection.getAll().forEach( (item) => {
    
    // console.log( "item" , item );
    
    if( "tags" in item.data ){

      // console.log( "item.data", item.data );
      
      let tags = item.data.tags;

      //// console.log("UNFILTERED tags: " , tags );

      tags = tags.filter( ( item ) => {
        if( item === "all" || item === "nav" || item === "post" || item === "posts" || item === "admin" ){
        // if( item === "all" || item === "nav" || item === "post" || item === "posts" ){
          return false;
        }
        return true;
      } );

      //// console.log("FILTERED tags: " , tags );

      for( const tag of tags ){
        tagSet.add( tag );
      }
    }

  } );

  const uniqueArray = [...tagSet];
  //// console.log( "uniqueArray: " , uniqueArray );

  return uniqueArray;

} );