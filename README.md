# Simple Recipes 11ty


[![Netlify Status](https://api.netlify.com/api/v1/badges/594031d2-6ea8-41c7-a133-2d183ac89a6f/deploy-status)](https://app.netlify.com/sites/simple-recipes-11ty/deploys)


## Please visit
Eventually, you should be able to visit [the lesson at netlify](https://simple-recipes-11ty.netlify.app "Link to Simple Recipes 11ty") and decide if this is something for you to play around with at a later date.

## Description
Simple recipes static content presentation made easy with 11ty.

## Attributions and dedications
These are presented in no particular order.

Members of my own immediate family such as:
  - Dad (who has gone to be with the Lord) for the homemade beans recipe, as well as, the ovenbaked rice recipe;
  - Mom for the homemade spaghetti sauce recipe;
  - My &quot;baby&quot; sister, Cristy (who has gone to be with the Lord) for sharing her unconditional love with me, her exemplary display of grace under pressure, and her bravery during her life in experimenting with some of these recipes as I was discovering different dishes;

I had conducted research on the subject, placement, and application of an &quot;eleventy baseurl&quot;
  - [Turn Jekyll Up to Eleventy](https://24ways.org/2018/turn-jekyll-up-to-eleventy/ "link to 24ways")
  - [From Jekyll to Eleventy](https://www.webstoemp.com/blog/from-jekyll-to-eleventy/ "link to Webstoemp")

The **eleventy docs** have a lot of energy and are similar to the **sailsjs docs**&#45;&#45; it&rsquo;s as if the author(s) took a big pull from a phillie blunt or sucked on a Tylenol gelcap laced with mezcaline prior to writing the docs. The docs are not concise, they lack clarity, and are super difficult to read. I would venture to say that without prior experience in playing with **Jekyll RB**, you will find that picking up **eleventy** is going to require more than a mere weekend in order to get acquainted with the subject.

Despite the initial constraints, the 11ty docs can offer the following advice; other better organized blogs offer some golden advice, too:
  - [404 page functionality](https://www.11ty.dev/docs/quicktips/not-found/ "link to docs")
  - [Opt&#45;out of using .gitignore](https://www.11ty.dev/docs/ignores/#opt-out-of-using-.gitignore "link to docs")
  - [Minified html output](https://www.11ty.dev/docs/config/#transforms "link to docs")
  - Read more on **shortcode** [here](https://www.11ty.dev/docs/shortcodes/ "link to docs") and [here](https://www.11ty.dev/docs/languages/liquid/#shortcode "link to docs")
  - [Plugin(s) - eleventy navigation](https://www.11ty.dev/docs/plugins/navigation/ "link to docs")
  - [Data deep merge](https://www.11ty.dev/docs/data-deep-merge/ "link to docs"): this goes hand in hand with navigation
  - Read more on **SASS compilation** [here](https://dev.to/mathieuhuot/processing-sass-with-11ty-5a09 "link to docs") **especially** or [here](https://www.belter.io/eleventy-sass-workflow/ "link to docs")
  - [Add Your Own Watch Targets](https://www.11ty.dev/docs/config/#add-your-own-watch-targets "link to docs") (SASS compilation, especially, came to mind prior to utilizing Chokidar for adequate file watching functionality)
  - [Manual Passthrough File Copy](https://www.11ty.dev/docs/copy/#manual-passthrough-file-copy-(faster) "link to docs")
  - [Filters](https://www.11ty.dev/docs/filters/ "link to docs")
  - [Eleventy Base Blog](https://github.com/11ty/eleventy-base-blog/ "link to 11ty base blog at github")

