const fs = require("fs");

const htmlDateString = require("./src/filters/htmlDateString");
const humanReadableDate = require("./src/filters/humanReadableDate");

const minifyHTML = require("./src/transforms/minifyHTML");

const pluginNavigation = require("@11ty/eleventy-navigation");

const getTagList = require("./src/_11ty/getTagList");

const asyncsass = require("./src/config/sass-promise-process-serve");

module.exports = ( ( eleventyConfig ) => {

  // ASYNC SASS COMPILATION 
  asyncsass( "src/sass/styles.scss" , "src/css/styles.css" ); 
  
  // addPassthroughCopy - PROPOSED PRODUCTION
  
  // Copy `src/css` to `dist/css`
  eleventyConfig.addPassthroughCopy( { "src/css" : "css" } ); 

  // Copy `src/passthru/img/` to `dist/assets/images`
  eleventyConfig.addPassthroughCopy( { "src/img": "assets/images" } ); 

  // Add Custom Watch Targets (e.g. -- trigger a build upon new and subsequent SASS compilations)
  // eleventyConfig.addWatchTarget( "src/sass/**/*.scss" ); // COUNTERINTUITIVE IN PRACTICE

  // CUSTOM FILTERS
  eleventyConfig.addFilter( "customHtmlDateString" , htmlDateString );
  eleventyConfig.addFilter( "customReadableDate" , humanReadableDate );

  // Data deep merge
  eleventyConfig.setDataDeepMerge( true );
  
  // 404 functionality
  eleventyConfig.setBrowserSyncConfig( { 
    callbacks: {
      ready: function( err, bs ){
        bs.addMiddleware( "*", (req,res) => {          
          const fourohfourstring = 'dist/404.html'; 
          const content_404 = fs.readFileSync(fourohfourstring);
          res.write(content_404); 
          res.writeHead(404);
          res.end(); 
        } );
      }
    }
  } );

  // HTML minification
  eleventyConfig.addTransform( "htmlmin", minifyHTML );

  // PLUGIN(S)- e.g. -- @11ty/eleventy-navigation
  eleventyConfig.addPlugin(pluginNavigation);

  // CUSTOM COLLECTIONS
  eleventyConfig.addCollection( "tagList" , getTagList );

  // SITE CONFIGURATION
  return {
    dir: {
      input: "src" // input: "."
      , output: "dist" // , output: "_site"
      , includes: "_includes"
      , layouts: "_layouts"
    }
  };

} );